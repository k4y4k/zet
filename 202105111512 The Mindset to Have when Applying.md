---
title: The Mindset to Have when Applying
date: 2021-05-11 15:12
---

- "What if I don't have enough experience?"
- "What if I'm underqualified for this job?"

Think about it like this instead.

If a job is asking you to do something you're already comfy doing, then where's the room to grow in the future?

Flip the worrying around. Instead, ask these questions:

- "What am I going to learn in this job?"
- "Is this job going to put things on me resume that will help me advance in the future?"

Keep in mind job postings are designed to weed out the weak. A lot of these will be created by non-technical people... or written to make the job sound way tougher than it is.

Every application should be a long shot. Give yourself room to grow.

---

Neagoie, A. (2020, December). 10. What if I Don’t Have Enough Experience? Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/

---

#jobsearch #mindset
