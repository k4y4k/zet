---
title: What Companies are Looking For
date: 2021-05-24 11:19
---

Companies are assessing candidates on 4 criteria:

# Analytic Skills

How can you think through a problem and analyse things? What's your thought process? How exactly do you go from problem to solution?

# Coding Skills

Do you code well? Is your code clean? Well organised? Can others understand it?

# Technical Skills

Do you know the fundamentals? Do you understand the pros and cons of any solutions?

# Communication Skills

Does your personality match the company's personality? Can you communicate well with others? Will you fit well within the company?

---

Neagoie, A. (2020, December). 50. What Are Companies Looking For? Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12214952

---

#interviewProcess #codinginterview #interview #mindset
