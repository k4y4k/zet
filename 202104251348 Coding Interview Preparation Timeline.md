---
date: 2021-04-25 13:48
title: Coding Interview Preparation Timeline
---

> The following map should give you an idea of how to tackle the interview preparation process ... it's not just about interview questions. Do projects and write code, too! [#mcdowell-CrackingCodingInterview2015]

# 1+ Years Before The Interview

- Build projects outside of school / work
- Learn multiple programming languages
- Expand your network
- Build website / portfolio showcasing your experience

# 3 - 12 Months Before the Interview

- Continue to work on projects. Try to add on one more project
- Create draft of resume and send it out for a resume review.
- Make list of preferred companies
- Read the intro sections of cracking the coding interview
- Learn and master [[202012011552 Big O]]
- Implement data structures and algorithms from scratch.
- Form mock interview group w/ friends to interview each other

# 1 - 3 Months Before the Interview

- Do mini-projects to solidify understanding of key concepts
- Do several mock interviews
- Continue to practice interview questions
- Create a list to track mistakes you’ve made solving problems

# 4 Weeks

- Create an [[202104251523 Interview Prep Grid]]
  - Review / update resume
  - Begin applying to companies
- Reread intro to cracking the coding interview, esp. tech / behavioural sections
- Do another mock interview
- Continue to practice questions, writing code on paper

# 1 week

- Phone interview: locate headset and / or video camera
- Do a final mock interview
- Rehearse stories from the [[202104251523 Interview Prep Grid]]
- Reread [[algorithm approaches]]
- Reread [[202012011552 Big O]]
- Continue to practice interview questions

# Day Before

- Rehearse each story from the prep grid once
- Continue to practice questions & review the list of mistakes
- Review [[powers of 2]]. Print it out for a phone screen

# Day of

- Wake up in time to eat a good breakfast and to be on time
- Be confident!! :)
- Remember to think out loud and show how you think
- Stumbling and struggling are normal

# Afterwards

- Write a thank you note to the recruiter
- Haven’t heard back in one week? Check in
- No offer? Ask when you can reapply. Don’t give up!!
- Offer? 🎉 Time to celebrate!

——-

[#mcdowell-CrackingCodingInterview2015]: McDowell, G. L. (2015). Cracking the Coding Interview: 189 Programming Questions and Solutions (6th edition). Palo Alto, CA: CareerCup.

——-

#code #coding #interview #codinginterview
