# Interview Prep Grid

> Go through each of the projects or components of your resume and ensure that you can talk about them in detail. [#mcdowell-CrackingCodingInterview2015]

The prep grid makes sure that you have enough to say about everything on your resume.

| Common Questions          | Project |
| ------------------------- | ------- |
| Challenges                |         |
| Mistakes / Failures       |         |
| Enjoyed                   |         |
| Leadership                |         |
| Conflicts                 |         |
| What you’d do differently |         |

---

[#mcdowell-CrackingCodingInterview2015]: McDowell, G. L. (2015). Cracking the Coding Interview: 189 Programming Questions and Solutions (6th edition). Palo Alto, CA: CareerCup.

---

#codinginterview
