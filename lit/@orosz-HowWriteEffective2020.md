---
title: How to write an effective developer resume: Advice from a hiring manager
authors: Gergely Orosz
year: 2020
---

# 1. Know What The Goal of Your Resume Is

The goal of your resume is to get a recruiter to call you back. That's it.

Resumes are not for relaying your _entire_ professional history.

Resumes are for selling yourself. "I'm the best hire for this position because ..." You do this by tuning it to the opening, by doing things like:

- changing the order of things
- adding specific, this-opening-only details

# 2. Use an easy-to-scan template

The typical read-through of a resume goes like this:

1. quick scan; if you match what they're looking for in the first 5 seconds, then
2. second read

A good template is:

- one column (easy top-to-bottom reading)
- dates / position name / company name are all separated. dates are top-to-bottom
- location and lang/techs are easy to find

# 3. Structure: Relevant Things First

# 4. Languages and Technologies: Be Crisp On What You Know

> Positions hiring for generalist software engineers will want to see some evidence that you have worked with a few technologies. For these places, it’s a good sign if you’ve mastered multiple technologies. Positions hiring for specific technologies will want to confirm that you did meaningful work with those technologies.

Do this by having a separate section for 'languages and technologies' on the first page of the resume;

```
languages: JS, HTML, ...
technologies: React, Bootstrap, ...
```

or calling out lang / tech used as part of work experience

```
job title | whatever
- created troubleshooting tools that improved the support teams efficiency by more than 30%. Build the tools with React and mongoDB.
```

# 5. Standing out: Results, Impact and Numbers

- use numbers
- use active language ( fixing bugs 👎️ reduced user reported defects 👍️, the first is passive)
- mention specific lang / tech where it makes sense

# 6. Tailor your resume to the position

> Having a “master” resume and tailoring it for every position you apply for is a great strategy. The tailoring doesn’t have to be drastic.

Job wants Node / JS? move Node / JS stuff up top. Small change, big impact.

# 7. Things That Will Help That Are Not Your Resume

- employee referrals

a referral makes the recruiter's job easier. no clue how to get one? ask. linkedin, twitter, blind maybe.

> You can try and reach out for a cold referral: but if you do, add context on why you think you are a great fit for a job and offer an easy way out for the person to not refer you if they do not feel comfortable doing so.

- cover letters are a bit of a coin toss.

| don't sweat it                       | give it a bit of tlc    |
| ------------------------------------ | ----------------------- |
| larger companies, corporate settings | smaller firms, startups |

don't repeat your resume - add additional details that sell you more. keep it short and relevant.

- never stop growing your professional network

help people, build genuine relationships with others, make yourself visible. Not just on social media, but irl too.

LinkedIn, GitHub and Stack are all good places to keep up-to-date.

- keep the linkedin up to date as you write your resume
- give your README a bit of love, as well as project READMEs
- tailor the Developer Story on Stack to make it easy for recruiters

---

#resume
