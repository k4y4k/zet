---
date: 202107141558
title: Last In, First Out
---

Data structures that are Last In, First Out (LIFO) can be visualised to be like a stack of plates.

You generally use the first plate on top of the stack before the others; you generally use the last datum entered into the LIFO-type structure before the others.

LIFO data structures:

- [[202107141549 Stacks]]

---

#dataStructures
