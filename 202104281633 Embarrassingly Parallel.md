---
title: Embarrassingly Parallel
date: 2021-04-28 16:33
---

When multiple workers can split up a problem and work in [[202104281605 Parallelism|parallel]] without too much hassle re: coordinating themselves, the problem is then called _embarrassingly parallel_.

An example of this is icing many cakes, or folding many boxes.

---

#parallelism
