---
title: Technical Skills
date: 2021-05-07 21:17
---

`Technical Skills` or `Languages and Technologies` is one of the core aspects of a developer resume.

> Positions hiring for generalist software engineers will want to see some evidence that you have worked with a few technologies. For these places, it’s a good sign if you’ve mastered multiple technologies. Positions hiring for specific technologies will want to confirm that you did meaningful work with those technologies. [#orosz-HowWriteEffective2020]

You can format it like

```
languages: JS, HTML, ...
technologies: React, Bootstrap, ...
```

or even put it in experience descriptions.

```
job title | whatever
- created troubleshooting tools that improved the support teams efficiency by more than 30%. Build the tools with React and mongoDB.
```

---

[#orosz-HowWriteEffective2020]: Orosz, G. (2020, November 25). How to write an effective developer resume: Advice from a hiring manager. Retrieved 7 May 2021, from Stack Overflow Blog website: https://stackoverflow.blog/2020/11/25/how-to-write-an-effective-developer-resume-advice-from-a-hiring-manager/

---

#resume
