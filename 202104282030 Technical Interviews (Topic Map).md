---
title: Technical Interviews (Topics Map)
date: 2021-04-28 20:30
---

An extension of [[202104282015 Coding Interview Prep (Topic Map)]]. Transcribed from a Coggle from Andrei Neagoie [#neagoie-MasterInterviewCourse2020].

- [[202012030840 Hash Tables]]
  - Improve time complexity?
    - Fast access at `O(1)`, uses more memory `O(n)`
  - Collision?
    - [[202012022017 Linked Lists]]
- [[Graphs]]
  - Shortest path?
    - Bellman-Ford
    - Dijikstra
  - Cyclic / acyclic
  - Weighted / Unweighted?
  - Graph traversal `O(n)`
    - Breadth First Search (BFS)
    - Depth First Search (DFS)
      - Inorder
      - Postorder
      - Preorder
  - Directed / Undirected
  - [[Tree]]
    - Tree Traversal `O(n)`
      - BFS
      - DFS
      - Recursion?
        - Be mindful of space complexity! stack overflow!
      - [[202012022017 Linked Lists]]
        - Singly Linked Lists
        - Doubly Linked Lists
      - Binary Tree
        - Binary Search Tree
          - Balanced BST
            - AVL Tree
            - Red Black Tree
      - Heap
        - Binary Heap
          - Priority Queue
      - Trie
- [[202012021931 (Static) Arrays]]
  - Sorting `O(n log n)`
    - Radix Sort
    - Quick Sort
    - Heap Sort
    - Bubble Sort
    - Selection Sort
    - Insertion Sort
    - Merge Sort
    - Counting Sort
  - String question?
    - Turn it into an array
  - [[202012021931 (Static) Arrays]]
  - [[202012021953 Dynamic Arrays]]
  - Searching
    - Is it sorted?
      - Yes
        - Divide and Conquer - Binary Search `O(log n)`
      - No
        - Will sorting make it faster?
          - Yes
            - Binary Search
          - No
            - Linear Search
        - Is it a string?
          - Try a Trie
- [[202107141549 Stacks]]
  - Array Stack
  - Linked List Stack
- [[202107141608 Queues]]
  - Array Queue ⚠️
  - Linked List Queue
- Dynamic Programming
  - Memoisation

---

[#neagoie-MasterInterviewCourse2020]: Neagoie, A. (2020). Master the Interview Course: Technical Interviews Topic Map. Retrieved 28 April 2021, from Coggle website: https://coggle.it/diagram/W5E5tqYlrXvFJPsq/t/master-the-interview-click-here-for-course-link

---

#interview #codinginterview #topic
