---
date: 202107141618
title: First In, First Out
---

Data structures that are First In, First Out (FIFO)-type are most like a queue from RCT2 (not to be confused with Queues the data structure).

Elements added to the queue are appended to the back (guests walking down the queue path). Elements that are removed from the queue leave from the front (guests being let into the rollercoaster).

FIFO-type data structures:

- [[202107141608 Queues]]

---

#dataStructures
