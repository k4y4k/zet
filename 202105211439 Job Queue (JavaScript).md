---
title: Job Queue (JavaScript)
date: 2021-05-21 14:39
---

The Job Queue is used by `Promise`s.

> Promises that resolve before the current function ends will be executed right after the current function. [#copes-JavaScriptEventLoop2018]

---

Copes, F. (2018, April 18). The JavaScript Event Loop. Retrieved 21 May 2021, from Flavio Copes website: https://flaviocopes.com/javascript-event-loop/

---

#javascript #programming #programmingTheory
