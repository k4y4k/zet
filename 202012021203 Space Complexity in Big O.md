---
title: Space Complexity in Big O
date: 2020-12-02 12:03
---

Algorithms often need to make a tradeoff between time and space / memory requirements. A slow, space-efficient algorithm might be better in one scenario, but a quick, memory-intensive one might suit another better.

It's always a balancing act.

When analysing space complexity, Big O considers the _additional_ space required by an algorithm, not including the initial size of [[202012021155 n]].

Steps that may introduce space complexity:

- variable assignment
- data structure use (arrays, objects, ...)
- calling functions

---

Neagoie, A. (2020, December). 40. Big O Cheat Sheet. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/

---

#programming #programmingTheory #bigO
