---
title: Melting Moments
date: 2021-08-24 13:41
---

| Prep | Cook     | Total    | Serves | Serving Size | kcal/serve |
| ---- | -------- | -------- | ------ | ------------ | ---------- |
| 5min | 15-20min | 20-25min | a few  | some bikkies | god knows  |

# Ingredients

## Bikkies

- 4 oz self-raising flour
- 2 oz custard powder
- 4 oz butter
- 1 1/2 oz icing sugar

## Icing

- 1 cup icing sugar
- 1/4 cup butter, melted

# Method

1. Preheat the oven to 160°ish C
2. Sift flour, custard powder and icing sugar together in a bowl
3. Work in the butter to create a dough
4. Shape the dough into balls. They won't grow very much.
5. Put the dough on a lined baking tray and press down lightly with a fork.
6. Bake for 15 - 20 minutes.
7. Remove biscuits to cool.
8. Once the biscuits are cooled, beat icing sugar and butter together to make icing. Put a bit of icing in between two biscuits to stick them together and let rest to set.

---

#recipe #biscuits
