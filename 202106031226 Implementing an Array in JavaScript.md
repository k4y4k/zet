---
title: Implementing An Array in JavaScript
date: 2021-06-03 12:26
---

> Arrays in JavaScript are just objects with integer-based keys that act like indexes. [#neagoie-67ImplementingArray2020]

```javascript
class MyArray {
  constructor() {
    // the length of the array, which is 0 when it's first created
    this.length = 0

    // the data "inside" the array: nothing when it's first initialised
    this.data = {}
  }

  /**
   * Returns the data at the specified index of the array.
   * @param index {number} the index of data to get
   *
   * @returns data {any}
   */
  get(index) {
    return this.data[index]
  }

  /**
   * Adds new data to the end of the array.
   * @param item {any} the new data to add to the array
   *
   * @returns length {number} the length of the array after the addition
   */
  push(item) {
    this.data[this.length] = item
    this.length++
    return this.length
  }

  /**
   * Delete the last item in the array
   */
  pop() {
    const lastItem = this.data[this.length - 1]
    delete this.data[this.length - 1]
    this.length--
    return lastItem
  }

  /**
   * Delete the item at the specified index
   */
  delete(index) {
    const item = this.data[index]
    this.shiftItems(index)
  }

  /**
   * Shift items towards the start to stop gaps from appearing in the array.
   */
  shiftItems(index) {
    for (let i = index; i < this.length - 1; i++) {
      this.data[i] = this.data[i + 1]
    }

    // Delete the last item - the for loop stops just before the last item,
    // leaving it otherwise untouched.
    delete this.data[this.length - 1]
    this.length--
  }
}
```

---

Creating a new `MyArray`:

```javascript
const newArray = new MyArray()
console.log(newArray.get(0)) // undefined - there is no data
```

Pushing an item into `MyArray`:

```javascript
const newArray = new MyArray()
newArray.push('hi')
console.log(newArray)
// ・> MyArray { length: 1, data: {0: "hi" } }
```

Deleting the last item from `MyArray`:

```javascript
const newArray = new MyArray()
newArray.push('hi')
newArray.push('there')
newArray.push('stranger')
console.log(newArray)
// ・> MyArray { length: 3, data: { 0: "hi", 1: "there", 2: "stranger" } }

newArray.pop()
console.log(newArray)
// ・> MyArray { length: 2, data: { 0: "hi", 1: "there" } }
```

Deleting an arbitrary item from `MyArray`:

```javascript
const newArray = new MyArray()
newArray.push('hi')
newArray.push('you')
newArray.push('!')
newArray.delete(0)
newArray.push('are')
newArray.push('nive')
newArray.delete(1)
// ・> MyArray { length: 3, data: { 0: "you", 1: "are", 2: "nice" } }
```

---

Neagoie, A. (2020, December). 67. Implementing An Array. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12301306

---

#javascript #programming
