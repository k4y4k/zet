---
title: Hash Table From Scratch
date: 2021-06-16 16:00
---

```javascript
class HashTable {
  constructor(size) {
    this.data = new Array(size)
  }

  // underscore = "hey this is a private method, don't use pls"
  _hash(key) {
    let hash = 0
    for (let i = 0; i < key.length; i++) {
      hash =
        // add the character code of the current character (0-65535) multiplied by the index
        (hash + key.charCodeAt(i) * i) %
        // mod that by the capacity of the hash table to prevent out of bounds
        this.data.length
    }
    return hash
  }

  set(key, value) {
    // hash the key
    let address = this._hash(key)
    // lookup:
    // if a bucket doesn't exist, create one and add it to the table
    if (!this.data[address]) {
      this.data[address] = []
      this.data[address].push([key, value])
    }

    // push the data we want to store to the correct bucket
    this.data[address].push([key, value])
    return this.data
  }

  get(key) {
    // lookup
    let address = this._hash(key)
    const currentBucket = this.data[address]

    // if there's a bucket at the key:
    if (currentBucket) {
      // loop through the bucket (which could contain 1+ elements)
      for (let i = 0; i < currentBucket.length; i++) {
        // return the value with a matching key
        if (currentBucket[i][0] === key) return currentBucket[i][1]
      }
    }

    // or if there's nothing:
    return undefined
  }

  keys() {
    const keysArray = []
    // iterate over every space
    for (let i = 0; i < this.data.length; i++) {
      // if a bucket exists in the space
      if (this.data[i]) {
        // extract its key
        keysArray.push(this.data[i][0][0])
      }
    }
    // finally, list the extracted keys
    return keysArray
  }
}

const myHashTable = new HashTable(50)
myHashTable.set('grapes', 10000)
myHashTable.set('apples', 23)
myHashTable.set('oranges', 4)
myHashTable.set('bananas', 9)
console.log(myHashTable.get('apples')) //・> 23
console.log(myHashTable.keys())
```

---

Neagoie, A. (2020a, December). 80. Solution: Implement A Hash Table. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12312672

Neagoie, A. (2020b, December). 81. Keys(). Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12314436

---

#programming #programmingTheory
