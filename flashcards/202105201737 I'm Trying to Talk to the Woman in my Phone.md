---
date:202105201737
title: I'm Trying to Talk to the Woman in my Phone
---

TARGET DECK
glorious nihongo

もちろんですよ::of course
<!--ID: 1621496349832-->

おやすみなさい::good night
<!--ID: 1621497711931-->

くもり::cloudy
<!--ID: 1621497711962-->

ひる::daytime
<!--ID: 1621497711979-->

あさごはん::breakfast
<!--ID: 1621497711986-->

さとう::sugar
<!--ID: 1621498059010-->

建築::(けんちく) architecture
<!--ID: 1621499238053-->

[...]を開く::(... をひらく) open [...]
<!--ID: 1621502297609-->

次の曲::(つぎのきょく) next song
<!--ID: 1621503696269-->

---

#日本語 #japanese #vocab #sentences
