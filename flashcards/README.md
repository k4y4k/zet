This folder is dedicated specifically to creating flashcards with the [Obsidian_to_Anki](https://github.com/Pseudonium/Obsidian_to_Anki) script.

# Card Types

The settings are as follows:

## Basic and reversed card

`^(.*[^\n:]{1}):{2}([^\n:]{1}.*)`

Matches:

```
This is the front of the card::This is the back
```

## Basic (type in the answer)

`\|([^\n|]+)\|\n\|(?:[^\n|]+)\|\n\|([^\n|]+)\|\n?`

Matches:

```
| This is the front of the card |
| ----------------------------- |
| This is the back of the card  |
```

## Cloze

`((?:.+\n)*(?:.*{.*)(?:\n(?:^.{1,3}$|^.{4}(?<!<!--).*))*)`

Matches:

```
This is the front of the card, and {this is the back}.
```

# Decks

Decks are synced w/ AnkiWeb.

| Name               | For                                                  |
| ------------------ | ---------------------------------------------------- |
| `glorious nihongo` | learning Japanese                                    |
| `hack into nasa`   | Comp. Sci. topics - data structures, algorithms, etc |
| `wow websites`     | JavaScript                                           |
