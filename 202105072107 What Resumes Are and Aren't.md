---
title: What Resumes Are and Aren't
date: 2021-05-07 21:07
---

Resumes are a way to get a recruiter to call you back. That's it.

They're not the memoirs of your professional life. Nobody has gotten a job off the back of a resume, so treat them accordingly – resumes are simply a way to _get interviews_ (and they’re not the _only_ way, either).

Resumes are for selling yourself. You have to convince whoever's on the other side of the application process that you're a good fit for the role!

---

#resume
