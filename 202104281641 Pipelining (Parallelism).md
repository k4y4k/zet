---
title: Pipelining (Parallelism)
date: 2021-04-28 16:41
---

If a non-parallelisable task has prerequisites (of any kind), you can perform the prerequisites of a later iteration of a task while waiting for a former's blocking step to complete.

An example of this is baking a lot of cookies. You can prepare, cut and arrange the dough on a baking sheet while a previous batch of cookies are baking in the oven.

---

#parallelism
