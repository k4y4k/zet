---
title: Symbol (JavaScript)
date: 2021-05-21 13:23
---

A Symbol is a value that is guaranteed to be unique.

```javascript
let sym1 = Symbol()
let sym2 = Symbol('foo')
let sym3 = Symbol('foo')
```

`sym2` and `sym3` are not the same:

```javascript
Symbol('foo') === Symbol('foo') // false
```

`Symbol("text")` is just a way to label Symbols (where `text` is the label).

and you can't use the `new` operator:

```javascript
let sym = new Symbol() // 🚨 TypeError
```

You can use Symbols in objects like so (note the use of `[]`):

```javascript
let userId = Symbol('id')

let employee = {
  name: 'Parwinder',
  [userId]: 727,
}

console.log(employee[userId]) // 727
console.log(employee['userId']) // undefined
```

Symbols **do not** show up in `for... in` loops.

You can use them to create 'hidden' properties on an object:

```javascript
let car = {
  name: 'BMW',
}

let hiddenField = Symbol('price')

car[hiddenField] = 70000

console.log(car) // { name: 'BMW', [Symbol(price)]: 70000 }
console.log(car[hiddenField]) // 70000
```

---

Mozilla Developer Network contributors. (2021, May 5). Symbol. Retrieved 21 May 2021, from Mozilla Developer Network website: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol

Parwinder 👨🏻‍💻. (2020, September 4). JavaScript Types: Symbol. Retrieved 21 May 2021, from DEV Community website: https://dev.to/bhagatparwinder/javascript-types-symbol-52di

---

#javascript #es6 #syntax #programming
