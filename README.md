---
date: 1970-01-01 00:00
title: In The Beginning (README)
---

![Used With: Obsidian](https://img.shields.io/badge/used%20with-obsidian-purple?style=for-the-badge)

Figured I might as well publish my Zettelkästen (which I have affectionately nicknamed `Zetty Betty`) online. Take a peek if you like.

# Citation Style

Citations / references in this document are generated with Zotero and the Better Bibtex plugin. Citekeys are generated with this template: `[authors2+-:lower:nopunct]-[shorttitle3_3][year]`. This results in keys like `test-ImpressiveTitle2020`

The bibliography style used here is APA 6th (comes default with Zotero). A BibLaTeX file (`zet.bib`) is included, because... because.

# Subfolders

Ideally, the Zettelkästen remains in the root of this repo. However, some exceptions exist:

- Literature notes (i.e. I have nvim open to take notes as I watch a video or read an article) are in `lit/`
- Images (the few that there are) are in `img/`
