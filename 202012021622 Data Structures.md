---
title: Data Structures
date: 2020-12-02 16:22
---

> A data structure is a collection of values. Algorithms are the steps we put into place to manipulate these collections of values. [#neagoie-58SectionOverview2020]

> The values [in a data structure] can have relationships among them, and they can have functions applied to them. Each one is different in what it can do and what it is best used for. [#neagoie-59WhatData2020]

Here's some:

- [[202012021931 (Static) Arrays]]
  - [[202012021943 Pointers (in Arrays)]]
  - [[202012021953 Dynamic Arrays]]
- [[202012021924 Fixed-Width Integers]]
- [[202012022017 Linked Lists]]
- [[202012030840 Hash Tables]]

---

Neagoie, A. (2020, December). 58. Section Overview. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12298518

Neagoie, A. (2020, December). 59. What Is A Data Structure? Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12298740

---

#programming #programmingTheory #dataStructures
