---
title: Rules to Solve Coding Interviews
date: 2021-05-24 14:07
---

- It works
- Good use of data structures
- Code Re-use/ Do Not Repeat Yourself
- Modular - makes code more readable, maintainable and testable
- Less than O(N^2). We want to avoid nested loops if we can since they are expensive. Two separate loops are better than 2 nested loops
- Low Space Complexity --> Recursion can cause stack overflow, copying of large arrays may exceed memory of machine

---

Neagoie, A. (2020, December). 52. Exercise: Google Interview. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12214968

---
