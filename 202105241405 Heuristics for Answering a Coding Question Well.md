---
title: Heuristics for Answering a Coding Question Well
date: 2021-05-24 14:05
---

- Hash Maps are usually the answer to improve Time Complexity
- If it's a sorted array, use Binary tree to achieve O(log N). Divide and Conquer - Divide a data set into smaller chunks and then repeating a process with a subset of data. Binary search is a great example of this
- Try Sorting your input
- Hash tables and precomputed information (i.e. sorted) are some of the best ways to optimize your code
- Look at the Time vs Space tradeoff. Sometimes storing extra state in memory can help the time. (Runtime)
- If the interviewer is giving you advice/tips/hints. Follow them
- Space time tradeoffs: Hastables usually solve this a lot of the times. You use more space, but you can get a time optimization to the process. In programming, you often times can use up a little bit more space to get faster time

---

Neagoie, A. (2020, December). 52. Exercise: Google Interview. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12214968

---
