---
date: 202107191737
title: Linked Lists vs Hash Tables
---

[[202012022017 Linked Lists]] have some overlap with [[202012030840 Hash Tables]], but there are some differences to keep in mind.

Linked Lists enforce an order, where Hash Tables do not.

Hash Tables (and Arrays (both types)) are congruent in memory, where Linked Lists are scattered `[data, next]` 'buckets'.

Linked Lists and Hash Tables must both perform traversal through the structure for lookup, except in the case of retrieving a value via a key with a Hash Tables.

---

#programming #dataStructures
