---
title: Call Stack (JavaScript)
date: 2021-05-21 14:34
---

The call stack is a component of the [[202105211352 JavaScript Event Loop]].

The call stack is a LIFO queue. Functions and expressions are added to the top as they're encountered, and the stack executes top-down.

That's what makes loops work: the contents of the loop are added to the call stack on top of the loop expression, and therefore the loop is only removed from the stack once it no longer needs to iterate.

---

Copes, F. (2018, April 18). The JavaScript Event Loop. Retrieved 21 May 2021, from Flavio Copes website: https://flaviocopes.com/javascript-event-loop/

---

#javascript #programming #programmingTheory
