---
date: 202108091324
title: Baby's First Linked List
---

JavaScript doesn't have linked lists built-in to the language.

But _since when_ has that stopped any of the mad scientists?

```javascript
class LinkedList {
  // It's the constructor's job to create the first node. Even if we later add
  // to the list, at the time of creation, it's still a length 1 null-terminated
  // list.
  constructor(value) {
    // each node has:
    // 1. the data of the node
    // 2. the location of the next node in the linked list
    this.head = {
      value,
      next: null,
    }
    // The list only has one item, so the tail IS the head (to begin with)
    this.tail = this.head
    this.length = 1
  }

  append(value) {
    const newNode = { value, next: null }

    // append to the end of the linked list
    this.tail.next = newNode
    // update the tail to point to the newly created node
    this.tail = newNode
    // the list is 1 item longer now
    this.length++

    return this
  }

  prepend(value) {
    // next is the current head
    const newNode = { value, next: this.head }

    // make the new node the new head
    this.head = newNode
    // we have added to the linked list
    this.length++

    return this
  }

  printList() {
    const array = []
    let currentNode = this.head
    while (currentNode !== null) {
      array.push(currentNode.value)
      currentNode = currentNode.next
    }

    return array
  }

  traverseToIndex(index) {
    // check index is valid
    if (typeof index !== 'number')
      throw new Error('index passed into traverseToIndex() was not a number')

    let counter = 0
    let currentNode = this.head

    // traverse through the list
    while (counter !== index) {
      currentNode = currentNode.next
      counter++
    }

    // return the node at the index we want
    return currentNode
  }

  insert(index, value) {
    // check index is a number
    if (typeof index !== 'number')
      throw new Error('index passed into insert() was not a number')

    // if the passed index is bigger than the list is long, just add the value
    // to the end of the list
    if (index >= this.length) return this.append(value)

    // if we want to insert a value into the beginning of a list
    if (index === 0) {
      this.prepend(value)
      return this.printList()
    }

    const newNode = { value, next: null }

    // first, get the leader. The leader is the node BEFORE where we want to
    // insert our new node.
    const leader = this.traverseToIndex(index - 1)

    // save a reference to the leader's trailing node
    const holdingPointer = leader.next

    // insert the new node after the leader
    // this disconnects the list
    leader.next = newNode

    // join the new node to the node originally after the leader
    // this rejoins the list
    newNode.next = holdingPointer

    // the list is now one node longer
    this.length++

    return this.printList()
  }

  remove(index) {
    if (typeof index !== 'number')
      throw new Error(
        'index passed to remove() was not a number, recieved ' +
          index +
          ' instead.'
      )

    // find the node before the one we want deleted
    const leader = this.traverseToIndex(index - 1)

    // we don't want the node that the leader currently points to
    const unwantedNode = leader.next

    // join the leader and the node after the one we are removing.
    // the unwanted node thus becomes detached from the list and (in JavaScript at least)
    // is garbage collected away
    leader.next = unwantedNode.next

    // the list is now one item shorter
    this.length--

    return this.printList()
  }
}

const myLinkedList = new LinkedList(10)
myLinkedList.append(5)
myLinkedList.prepend(99)
myLinkedList.insert(2, 'abc')
myLinkedList.insert(200, 'xyz')
myLinkedList.insert(0, 'and a 1 2 3:')
console.log(myLinkedList.printList())

myLinkedList.remove(2)
console.log(myLinkedList.printList())
```

---

#programming #exercise
