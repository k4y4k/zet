---
title: The Event Loop
date: 2021-05-21 13:52
---

JavaScript is single-threaded; we don't need to worry about concurrency issues. What we do need to worry about instead is not doing things that block the thread, like network operations that aren't `async`, or infinite loops.

Most of the time, each tab has its own event loop. This isolates processes and stops a badly-made web page from locking up the entire browser.

The event loop continuously monitors the [[202105211434 Call Stack (JavaScript)]]to see if there's any work it needs to do.

The call stack is a LIFO queue. Functions and expressions are added to the top as they're encountered, and the stack executes top-down.

That's what makes loops work: the contents of the loop are added to the call stack on top of the loop expression, and therefore the loop is only removed from the stack once it no longer needs to iterate.

---

Copes, F. (2018, April 18). The JavaScript Event Loop. Retrieved 21 May 2021, from Flavio Copes website: https://flaviocopes.com/javascript-event-loop/

---

#javascript #programming #programmingTheory
