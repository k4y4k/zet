---
title: Results, Not Responsibilities
date: 2021-05-07 18:27
---

Numbers make an item of experience look good on your resume. They quantify stuff. For example, if you were a footballer, you could say:

> _Responsible for directing on-field offense of professional football team_

or, it could be:

> _Lead Town Team offense to an 11-5 season, and to the championship._

Quantifying `11 wins` is much better than just saying `directed offense`. Throwing in some action words never hurt anyone either.

---

> Start keeping track of your own stats. Start today and look around you. Think about "how many" for all the things that are part of your workday, and put them on your resume. [#lester-TrackYourProfessional2011]

- How many people on your team?
- How many lines of code in the codebase?
- How many users use your software?
- How many users on your network? How many servers? How much storage?
- How many support calls do you take per day? Per week?
- How much money has your work saved the company?

---

[#lester-TrackYourProfessional2011]: Lester, A. (2011, November 14). Track your professional stats like a pro athlete to give your resume power. Retrieved 7 May 2021, from Andy Lester’s blog website: https://blog.petdance.com/2011/11/14/track-your-stats-like-a-pro-athlete-to-give-your-resume-power/

---

#resume
