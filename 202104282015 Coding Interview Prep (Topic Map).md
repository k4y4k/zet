---
title: Coding Interview Prep (Topic Map)
date: 2021-04-28 20:15
---

Consider this a broad overview. Courtesy of Andrei Neagoie [#neagoie-MasterInterviewCourse]. Also related: [[202104251348 Coding Interview Preparation Timeline]].

- Master the Interview
  - Getting the Interview
    - Resume (Cover letters)
    - How to make up for little experience
    - LinkedIn
    - Portfolio
    - Email
    - Where to find jobs?
    - When are you ready to apply?
  - [[202012011552 Big O]]
  - [[202104282030 Technical Interviews (Topic Map)]]
  - Offer + Negotiation
    - Handling an offer
    - Handling multiple offers
    - Handling rejection
    - Getting a raise
    - Negotiation 101
  - Google Questions
  - Amazon Questions
  - Facebook Questions
  - Domain Specific Questions
  - Top Interview Questions
  - Non Technical Interviews
    - Tell me about a problem you solved?
    - Why do you want to work for us?
    - What to ask the Interviewer
    - Tell me about yourself?
    - What is your biggest weakness?
    - Secret Weapon

---

#interview #codinginterview #udemy #topic

---

[#neagoie-MasterInterviewCourse]: Neagoie, A. (n.d.). Master the Interview Course Topic Map. Retrieved 28 April 2021, from https://coggle.it/diagram/W5u8QkZs6r4sZM3J/t/master-the-interview
