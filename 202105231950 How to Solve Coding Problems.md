---
title: How to Solve Coding Problems
date: 2021-05-23 19:50
---

> At the end of the day, the interview is a way for the company to find out: can _you_ solve a problem that the company or employer has? Because if you're able to solve their problems... then you're valuable, and you will produce more value to them than the salary they're paying to you. [#neagoie-49SectionOverview2020]

A good place to start is probably knowing [[202105241119 What Companies are Looking For]].

These are considered the must-knows for interviews:

| [[202012021622 Data Structures]] | Algorithms          |
| -------------------------------- | ------------------- |
| [[202012021931 (Static) Arrays]] | Sorting             |
| Stacks                           | Dynamic Programming |
| Queues                           | BFS                 |
| [[202012022017 Linked Lists]]    | DFS                 |
| Trees                            | Recursion           |
| Tries                            |                     |
| Graphs                           |                     |
| [[202012030840 Hash Tables]]     |                     |

There is a [[202105241211 Process to Solve Coding Interviews]].

> In interviews, you should treat any string questions as an array question — strings are simply an array of characters. [#neagoie-68StringsArrays2020]

---

[#neagoie-49SectionOverview2020]: Neagoie, A. (2020, December). 49. Section Overview. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12214950

Neagoie, A. (2020, December). 51. What We Need For Coding Interviews. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12214956

[#neagoie-68StringsArrays2020]: Neagoie, A. (2020, December). 68. Strings and Arrays. Udemy. Retrieved from https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/learn/lecture/12308636

---

#interview #interviewProcess
