---
date: 202108091215
title: First Recurring Character
---

Given an array like `[ 2, 5, 1, 2, 3, 5, 1, 2, 4 ]`, find the first recurring character (`2`).

`[ 2, 1, 1, 2, 3, 5, 1, 2, 4 ]` -> `1`

`[ 2, 3, 4, 5 ]` -> `undefined`

```javascript
const firstRecurring = arr => {
  let foundNums = {}

  for (let i = 0; i < arr.length; i++) {
    const current = arr[i]

    if (foundNums[current]) {
      return current
    } else {
      foundNums[current] = true
    }
  }
  return undefined
}

console.log(firstRecurring([2, 5, 1, 2, 3, 5, 1, 2, 4])) // 2
console.log(firstRecurring([2, 1, 1, 2, 3, 5, 1, 2, 4])) // 1
console.log(firstRecurring([2, 3, 4, 5])) //undefined
```

Time complexity of $O(n)$. Space complexity of $O(n)$.

---

#programming #exercise
